package com.ayushbagaria.agrostar.viewHolder;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.ayushbagaria.agrostar.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HListItemViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.hlist_iv)
    public ImageView hlistImageView;

    public HListItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
