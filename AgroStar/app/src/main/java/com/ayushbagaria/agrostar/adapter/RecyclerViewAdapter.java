package com.ayushbagaria.agrostar.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ayushbagaria.agrostar.R;
import com.ayushbagaria.agrostar.layoutManager.CustomLinearLayoutManager;
import com.ayushbagaria.agrostar.model.Seed;
import com.ayushbagaria.agrostar.viewHolder.HorizontalViewHolder;
import com.ayushbagaria.agrostar.viewHolder.SeedItemViewHolder;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final LayoutInflater inflater;
    private List<Seed> seedList = new ArrayList<>();
    private Context context;
    private int mode;

    public RecyclerViewAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addSeedList(List<Seed> seedList, int mode) {
        this.seedList.clear();
        this.seedList.addAll(seedList);
        this.mode = mode;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (mode == 0 && position == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view  = LayoutInflater.from(context).inflate(R.layout.horizontal_list_view, parent, false);
            return new HorizontalViewHolder(view);
        } else if (viewType == 1) {
            View view  = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
            return new SeedItemViewHolder(view);
        } else {
            return null;
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (mode == 0 && position == 0) {
            int[] images = {R.drawable.seeds1,R.drawable.seeds2,R.drawable.seeds3,R.drawable.seeds4,
            R.drawable.seeds5,R.drawable.seeds6,R.drawable.seeds7,R.drawable.seeds8,R.drawable.seeds9,
            R.drawable.seeds10,R.drawable.seeds11};
            CustomLinearLayoutManager linearmanager = new CustomLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            HorizontalViewAdapter hlistAdapter = new HorizontalViewAdapter(context,images);
            HorizontalViewHolder horizontalViewHolder = (HorizontalViewHolder)viewHolder;
            horizontalViewHolder.hlist_rv.setLayoutManager(linearmanager);
            horizontalViewHolder.hlist_rv.setAdapter(hlistAdapter);
            horizontalViewHolder.hlist_rv.setHasFixedSize(true);
        } else {
            int index;
            if (mode == 1) {
                index = position;
            } else {
                index = position -1;
            }
            SeedItemViewHolder seedItemHolder = (SeedItemViewHolder)viewHolder;
            seedItemHolder.tv_price.setText(seedList.get(index).getPrice());
            seedItemHolder.tv_quantity.setText(seedList.get(index).getQuantity());
            seedItemHolder.tv_seed_name.setText(seedList.get(index).getName());
            seedItemHolder.iv_crop.setImageResource(R.mipmap.ic_launcher);
        }
    }


    @Override
    public int getItemCount() {
        return mode==0?(seedList.size()+1):seedList.size();
    }



}

