package com.ayushbagaria.agrostar.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ayushbagaria.agrostar.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HorizontalViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.hlist_rv)
    public RecyclerView hlist_rv;

    public HorizontalViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
