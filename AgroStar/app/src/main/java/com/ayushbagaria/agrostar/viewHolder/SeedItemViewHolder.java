package com.ayushbagaria.agrostar.viewHolder;

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ayushbagaria.agrostar.R;

import org.w3c.dom.Text;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SeedItemViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.iv_crop)
    public ImageView iv_crop;
    @Bind(R.id.tv_seed_name)
    public TextView tv_seed_name;
    @Bind(R.id.tv_price)
    public TextView tv_price;
    @Bind(R.id.tv_quantity)
    public TextView tv_quantity;

    public SeedItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}


