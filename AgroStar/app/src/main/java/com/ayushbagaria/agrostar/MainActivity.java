package com.ayushbagaria.agrostar;

import android.app.SearchManager;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.ayushbagaria.agrostar.adapter.RecyclerViewAdapter;
import com.ayushbagaria.agrostar.layoutManager.CustomLinearLayoutManager;
import com.ayushbagaria.agrostar.model.Seed;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity  implements SearchView.OnQueryTextListener {
    @Bind(R.id.rv)
    public RecyclerView myRecyclerView;
    @Bind(R.id.toolbar)
    public Toolbar toolbar;
    public ArrayList<Seed> listData = new ArrayList<>();
    RecyclerViewAdapter adapter;
    Locale myLocale;
    public static final String MyPREFERENCES = "myPref";
    SharedPreferences sharedPreferences;
    Boolean activityCreated = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getIntent() != null && getIntent().getBooleanExtra("activitycreated",false)) {
            activityCreated = false;
        }
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        if(sharedPreferences != null && activityCreated) {
           String lang = sharedPreferences.getString("lang",null);
            if (lang == null){
                lang = "en";
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("lang",lang).apply();
            }
            setLocale(lang);
        } else {
            adapter = new RecyclerViewAdapter(this);
            try {
                getData();
            } catch (IOException e) {
                e.printStackTrace();
            }
            LinearLayoutManager custommanager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            myRecyclerView.setLayoutManager(custommanager);
            myRecyclerView.setHasFixedSize(true);
            myRecyclerView.setAdapter(adapter);
        }
    }

    private void getData() throws IOException {
        String jsonData;
        JsonParser parser = new JsonParser();
        InputStream iStream = getResources().openRawResource(R.raw.data);
        int size = iStream.available();
        byte[] buffer = new byte[size];
        iStream.read(buffer);
        iStream.close();
        jsonData = new String(buffer, "UTF-8");
        System.out.print(jsonData);
        Log.d("1", jsonData);
        listData = new Gson().fromJson(jsonData, new TypeToken<ArrayList<Seed>>() {
        }.getType());
        adapter.addSeedList(listData,0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.search);;
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        MenuItemCompat.setOnActionExpandListener(searchItem,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem menuItem) {
                        adapter.addSeedList(listData, 1);
                        return true;
                    }
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                       adapter.addSeedList(listData, 0);
                        return true;
                    }
                });
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        final ArrayList<Seed> filteredSeedList = filter(listData, query);
        adapter.addSeedList(filteredSeedList, 1);
        return true;
    }

    private ArrayList<Seed> filter(ArrayList<Seed> listData, String query) {
        query = query.toLowerCase();
        final ArrayList<Seed> filteredSeedList = new ArrayList<>();
        for (Seed model : listData) {
            final String text = model.getName().toLowerCase();
            if (text.contains(query)) {
                filteredSeedList.add(model);
            }
        }
        return filteredSeedList;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.change_lang) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("lang", getString(R.string.suffix)).apply();
            setLocale(getString(R.string.suffix));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setLocale(String lang) {
        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, MainActivity.class);
        refresh.putExtra("activitycreated",true);
        startActivity(refresh);
        finish();
    }
}
