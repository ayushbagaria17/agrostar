package com.ayushbagaria.agrostar.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ayushbagaria.agrostar.R;
import com.ayushbagaria.agrostar.model.Seed;
import com.ayushbagaria.agrostar.viewHolder.HListItemViewHolder;
import com.ayushbagaria.agrostar.viewHolder.HorizontalViewHolder;


public class HorizontalViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final LayoutInflater inflater;
    private int[] imagelist = new int[11];
    private Context context;

    public HorizontalViewAdapter(Context context,int[] imagelist) {
        this.context = context;
        this.imagelist = imagelist;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(context).inflate(R.layout.horizontal_list_item, parent, false);
        return new HListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HListItemViewHolder view = (HListItemViewHolder) holder;
        view.hlistImageView.setImageResource(imagelist[position]);
        view.hlistImageView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return imagelist.length;
    }
}
